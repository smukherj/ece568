#Suvanjan Mukherjee, 997952336, suvanjan.mukherjee@mail.utoronto.ca
#Max Holden, 998277047, max.holden@mail.utoronto.ca

Target 1
--------
Vulnerable to a stack smashing attack. 'foo' does a strcpy without bounds checking
from the command line argument to 'buf'. By making the command line argument big
enough, we can rewrite the return address of lab_main to point to the shellcode
stored in the buffer

Target 2
--------
Target 2 is vulnerable to a stack smashing attack. Although 'len' is originally
set to 272, which would not allow us to overwrite the return address of foo, we
first overwrite len, and then overwrite the return address of foo. We also had to
overwrite i so that the loop would continue/terminate properly.

Target 3
--------
This target is very similar to 1 and is also vulnerable to a stack smashing attack.
'bar' does bounds checking before doing the copy but 'foo' passes in a length
which is longer than the target buffer. This extra length is enough to overwrite
the return address of 'foo'

Target 4
--------
Target 4 is also vulnerable to a stack smashing attack. 'foo' keeps coying from
the command line argument into 'buf' for a length of '169' characters even though
the buffer is of size 156. This is enough to overwrite the values of 'i' and 'len'
so that the for loop continues until we can just overwrite the return address
of lab_main.

Target 5
--------
Target 5 is vulnerable to a format string attack. We pass in a string that contains
the location of the return address of foo, the shellcode, and the necessary formatters
(%x and %ns). As discussed in lecture, we overwrite the return address one byte at a
time (using %hhn), and use %nnx specifiers to construct our return address (which 
points to our shellcode).

Target 6
--------
Target 6 has a double free vulnerability in 'foo'. The address pointer to by q is
freed twice. We create a fake tag at q where the prev points to the beginning of
the buffer and the next points to the return address. We also set the free bit
in the beginning of the buffer so that only the left coalescing code of the tfree
comes into play. After 'tfree' of q the second time, the second 4 bytes of the
buffer get corrupted and we have a jump instruction in the beginning of the
buffer to jump over these 4 bytes to the shellcode.
