To generate a self-signed certificate:

1. openssl genrsa -des3 -out privkey.pem 2048
	-enter/create a passphrase
2. openssl req -new -x509 -key privkey.pem -out cert.pem -days 1095
	-enter passphrase and follow prompts
3. cat privkey.pem >> cert.pem

Use cert.pem as self-signed certificate.
