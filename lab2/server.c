#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "common.h"

#define PORT 8765

/* use these strings to tell the marker what is happening */
#define FMT_ACCEPT_ERR "ECE568-SERVER: SSL accept error\n"
#define FMT_VERIFY_ERR "ECE568-SERVER: SSL verify error\n"
#define FMT_CLIENT_INFO "ECE568-SERVER: %s %s\n"
#define FMT_OUTPUT "ECE568-SERVER: %s %s\n"
#define FMT_INCOMPLETE_CLOSE "ECE568-SERVER: Incomplete shutdown\n"

#define KEYFILE "bob.pem"
#define PASSWORD "password"
#define MAX_BUF_LEN 256

void show_client_cert(SSL *ssl){
	X509 *cert;
	char CN[MAX_BUF_LEN];
	char email[MAX_BUF_LEN];

	cert = SSL_get_peer_certificate(ssl);
	X509_NAME_get_text_by_NID(X509_get_subject_name(cert), 
															NID_commonName, CN, MAX_BUF_LEN);
	X509_NAME_get_text_by_NID(X509_get_subject_name(cert), 
															NID_pkcs9_emailAddress, email, MAX_BUF_LEN);

	/* print client info */
	printf(FMT_CLIENT_INFO, CN, email);
}

int tcp_listen(int port){
	int sock, val;
	struct sockaddr_in sin;

	if((sock=socket(AF_INET,SOCK_STREAM,0))<0){
    perror("socket");
    close(sock);
    exit(0);
  }

  memset(&sin,0,sizeof(sin));
  sin.sin_addr.s_addr=INADDR_ANY;
  sin.sin_family=AF_INET;
  sin.sin_port=htons(port);

	val = 1;
  setsockopt(sock,SOL_SOCKET,SO_REUSEADDR, &val,sizeof(val));

  if(bind(sock,(struct sockaddr *)&sin, sizeof(sin))<0){
    perror("bind");
    close(sock);
    exit (0);
  }

  if(listen(sock,5)<0){
    perror("listen");
    close(sock);
    exit (0);
  }

	return sock;
}

int main(int argc, char **argv)
{
  int s, sock, port;
  pid_t pid;
	int ret;

	/* SSL stuff */
	SSL_CTX *ctx;
	BIO *bio;
	SSL *ssl;

  /*Parse command line arguments*/
  port = PORT;
	switch(argc){
    case 1:
      break;
    case 2:
      port=atoi(argv[1]);
      if (port<1||port>65535){
				fprintf(stderr,"invalid port number");
				exit(0);
      }
      break;
    default:
      printf("Usage: %s port\n", argv[0]);
      exit(0);
  }

	/* setup the listen port */
	sock = tcp_listen(port);
	
	/* initialize SSL context */
	ctx = initialize_ctx(KEYFILE,PASSWORD);
	SSL_CTX_set_cipher_list(ctx, "SSLv2:SSLv3:TLSv1");
	SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT, 0);

	/* accept and handle incoming connections */
  while(1){

    if((s=accept(sock, NULL, 0))<0){
      perror("accept");
      close(sock);
      close(s);
      exit (0);
    }

    if((pid=fork())){
      close(s);
    }
    else {
      /*Child code*/
      int len;
      char buf[MAX_BUF_LEN];
      char *answer = "42";

			/* SSL stuff */
			bio = BIO_new_socket(s, BIO_NOCLOSE);
			ssl = SSL_new(ctx);
			SSL_set_bio(ssl, bio, bio);

			/* accept and verify */
			if(SSL_accept(ssl) <= 0)
      	berr_exit(FMT_ACCEPT_ERR);

			if(SSL_get_verify_result(ssl) == X509_V_OK){
				show_client_cert(ssl);
			} else{ /* invalid certificate */
				berr_exit(FMT_VERIFY_ERR);
			}

			/* read */
			len = SSL_read(ssl, &buf, MAX_BUF_LEN - 1);
			switch(SSL_get_error(ssl, len)){
				case SSL_ERROR_NONE: /* success */
					break;
				case SSL_ERROR_SYSCALL:
					printf(FMT_INCOMPLETE_CLOSE);
					goto out;
				default:
					printf("SSL_read: unkown error.\n");
					goto shutdown;
			}
			buf[len] = '\0';
			
			/* print client request and server response */
			printf(FMT_OUTPUT, buf, answer);
      
			/* write */
			ret = SSL_write(ssl, answer, strlen(answer));
			switch(SSL_get_error(ssl, ret)){
        case SSL_ERROR_NONE: /* success */
          break;
        case SSL_ERROR_SYSCALL:
          printf(FMT_INCOMPLETE_CLOSE);
          goto out;
        default:
          printf("SSL_write: unkown error.\n");
          goto shutdown;
      }

shutdown:
			ret = SSL_shutdown(ssl);
			switch(ret){
				case 1:
				case 0:
					break; /* success */
				case -1:
				default:
					printf(FMT_INCOMPLETE_CLOSE);
			}

out:
			SSL_free(ssl);
      close(sock);
      close(s);
      return 0;
    }
  }

	destroy_ctx(ctx);
  close(sock);
  return 1;
}
