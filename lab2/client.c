#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "common.h"

#define HOST "localhost"
#define PORT 8765

/* use these strings to tell the marker what is happening */
#define FMT_CONNECT_ERR "ECE568-CLIENT: SSL connect error\n"
#define FMT_SERVER_INFO "ECE568-CLIENT: %s %s %s\n"
#define FMT_OUTPUT "ECE568-CLIENT: %s %s\n"
#define FMT_CN_MISMATCH "ECE568-CLIENT: Server Common Name doesn't match\n"
#define FMT_EMAIL_MISMATCH "ECE568-CLIENT: Server Email doesn't match\n"
#define FMT_NO_VERIFY "ECE568-CLIENT: Certificate does not verify\n"
#define FMT_INCORRECT_CLOSE "ECE568-CLIENT: Premature close\n"

typedef struct
{
    int socket;
    SSL *ssl;
    SSL_CTX *ssl_ctx;
    BIO *ssl_bio;
} ssl_conn_t;

/**
* Return the tcp socket connected to
*/
int tcp_connect(const char *hostname, const int port)
{
    struct sockaddr_in addr;
    struct hostent *host_entry;
    int sock;

    /*get ip address of the host*/

    host_entry = gethostbyname(hostname);

    if (!host_entry){
        fprintf(stderr,"Couldn't resolve host");
        exit(0);
    }

    memset(&addr,0,sizeof(addr));
    addr.sin_addr=*(struct in_addr *) host_entry->h_addr_list[0];
    addr.sin_family=AF_INET;
    addr.sin_port=htons(port);

    debug("Info: Connecting to %s(%s):%d\n", hostname, inet_ntoa(addr.sin_addr)
    ,port);

    /*open socket*/

    if((sock=socket(AF_INET, SOCK_STREAM, IPPROTO_TCP))<0)
    {
        perror("socket");
    }
    if(connect(sock,(struct sockaddr *)&addr, sizeof(addr))<0)
    {
        perror("connect");
    }

    return sock;
}

void check_server_certificate(ssl_conn_t conn)
{
    X509 *peer;
    char peer_cn[256];
    char peer_email[256];
    char peer_issuer[256];

    if(SSL_get_verify_result(conn.ssl) != X509_V_OK)
    {
        berr_exit(FMT_NO_VERIFY);
    }

    peer = SSL_get_peer_certificate(conn.ssl);

    X509_NAME_get_text_by_NID ( X509_get_subject_name(peer),
                                NID_commonName,
                                peer_cn,
                                256);

    debug("Info: Server common name is '%s'\n", peer_cn);
    if(strcasecmp(peer_cn, "Bob's server"))
    {
        err_exit(FMT_CN_MISMATCH);
    }

    X509_NAME_get_text_by_NID ( X509_get_subject_name(peer),
                                OBJ_txt2nid("emailAddress"),
                                peer_email,
                                256);

    debug("Info: Server email address is '%s'\n", peer_email);
    if(strcasecmp(peer_email, "ece568bob@ecf.utoronto.ca"))
    {
        err_exit(FMT_EMAIL_MISMATCH);
    }

    X509_NAME_oneline(X509_get_issuer_name(peer), peer_issuer, 256);

    debug("Info: Server certificate issuer is '%s'\n", peer_issuer);

    printf(FMT_SERVER_INFO, peer_cn, peer_email, peer_issuer);

}

/**
* Return the conn_t object created.
*/
ssl_conn_t secure_connect(const char *hostname, const int port)
{
    ssl_conn_t conn;
    conn.socket = tcp_connect(hostname, port);

    // Now do the SSL stuff
	
		/* Uncomment one of the three initialization lines below */

    /* initialize with trusted certificate */
		conn.ssl_ctx=initialize_ctx("alice.pem","password");
		
		/* initialize with no certificate */
		//conn.ssl_ctx=initialize_ctx_no_cert();

		/* initialize with self-signed (untrusted) certificate */
		//conn.ssl_ctx=initialize_ctx("keys_certs/cert.pem", "password");

    // Requirement 1: Only connect to servers that support SSLv3 and TLSv1.
    // So remove SSLv1 and SSLv2.
    // But in https://www.openssl.org/docs/ssl/SSL_CTX_get_options.html I don't
    // see a way to remove SSLv1 so I'm guessing it is not required?
    SSL_CTX_set_options(conn.ssl_ctx, SSL_OP_NO_SSLv2);

    //Requirement 2: Only use the SHA1 hash function for the cipher
    SSL_CTX_set_cipher_list(conn.ssl_ctx,"SHA1");

    conn.ssl = SSL_new(conn.ssl_ctx);
    conn.ssl_bio = BIO_new_socket(conn.socket, BIO_NOCLOSE);
    SSL_set_bio(conn.ssl, conn.ssl_bio, conn.ssl_bio);

    if(SSL_connect(conn.ssl) <= 0)
    {
        berr_exit(FMT_CONNECT_ERR);
    }

    check_server_certificate(conn);

    return conn;
}

void exchange_data(ssl_conn_t conn)
{
    const char *secret = "What's the question?";
    char buf[256];

    int len = SSL_write(conn.ssl, secret, strlen(secret));

    switch(SSL_get_error(conn.ssl, len))
    {
        case SSL_ERROR_SYSCALL:
        case SSL_ERROR_SSL: berr_exit(FMT_INCORRECT_CLOSE); break;
        default: break;
    }

    len = SSL_read(conn.ssl, buf, 256);
    buf[len] = '\0';
    debug("Info: Client read %d bytes\n", len);
    switch(SSL_get_error(conn.ssl, len))
    {
        case SSL_ERROR_SYSCALL:
        case SSL_ERROR_SSL: berr_exit(FMT_INCORRECT_CLOSE); break;
        default: break;
    }

    printf(FMT_OUTPUT, secret, buf);
}

void cleanup_ssl_conn(ssl_conn_t conn)
{
    SSL_shutdown(conn.ssl);
    destroy_ctx(conn.ssl_ctx);
    close(conn.socket);
}

int main(int argc, char **argv)
{
    int port=PORT;
    char *host=HOST;

    /*Parse command line arguments*/

    switch(argc){
        case 1:
        break;
        case 3:
        host = argv[1];
        port=atoi(argv[2]);
        if (port<1||port>65535){
            fprintf(stderr,"invalid port number");
            exit(0);
        }
        break;
        default:
        printf("Usage: %s server port\n", argv[0]);
        exit(0);
    }

    ssl_conn_t conn = secure_connect(host, port);
    exchange_data(conn);
    cleanup_ssl_conn(conn);

    debug("Info: Shutting down client.\n");
    return 1;
}
